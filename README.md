## What is this?

This is a very basic implementation of a colour generator that provides aesthetically pleasing random colours.
This implementation is based on Martin Ankerl's [article.](https://martin.ankerl.com/2009/12/09/how-to-create-random-colors-programmatically/)

![Example](/example.png)

## How to use this library?

Simply add the following dependency


```html
<dependencies>
    ...
    <dependency>
         <groupId>com.ksidis</groupId>
         <artifactId>ColourGenerator</artifactId>
         <version>1.0</version>
    </dependency>
<dependencies>
```
### Examples 

To get a random colour use the following code and specify a saturation and a value (HSV)

```java
Color colour = ColourGenerator.getRandomColour(0.5f, 0.95f);
```

Alternatively you can rely on predefined palettes and call this

```java
Color colour = ColourGenerator.getRandomColour(Palette.PASTEL);
```
or

```java
Color colour = ColourGenerator.getRandomColour(Palette.VIVID);
```