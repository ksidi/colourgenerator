package com.ksidis;

import java.awt.*;

/**
 * A random colour generator
 *
 * Based on https://martin.ankerl.com/2009/12/09/how-to-create-random-colors-programmatically/
 *
 * @author Kostas Sidiropoulos <ksidis@gmail.com>
 */
public class ColourGenerator {

    private static final double GOLDEN_RATIO = 0.618_033_988_749_895;

    private ColourGenerator() {

    }

    public static Color getRandomColour(float saturation, float value) {
        double hue = Math.random() + GOLDEN_RATIO;
        hue %= 1;
        return Color.getHSBColor((float)hue, saturation, value);
    }

    public static Color getRandomColour(Palette palette) {
        double hue = Math.random() + GOLDEN_RATIO;
        hue %= 1;
        return Color.getHSBColor((float)hue, palette.getSaturation(), palette.getValue());
    }
}
