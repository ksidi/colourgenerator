package com.ksidis;

/**
 * @author Kostas Sidiropoulos <ksidiro@ebi.ac.uk>
 */
public enum Palette {
    VIVID(0.5f, 0.95f),
    LIGHT(0.3f, 0.99f),
    PASTEL(0.25f, 0.99f);

    private float saturation;
    private float value;

    Palette(float saturation, float value) {
        this.saturation = saturation;
        this.value = value;
    }

    public float getSaturation() {
        return saturation;
    }

    public float getValue() {
        return value;
    }
}
