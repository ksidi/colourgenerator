package com.ksidis;

import org.junit.Test;
import java.awt.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class ColourGeneratorTest {

    @Test
    public void generateTestHTMLPage() throws IOException {
        try(BufferedWriter writer = new BufferedWriter(new FileWriter("ColoursPage.html"))) {
            for(int i=0;i<30;i++) {
                Color c = ColourGenerator.getRandomColour(Palette.PASTEL);
                String hex = String.format("#%02x%02x%02x", c.getRed(), c.getGreen(), c.getBlue());
                writer.write("<span style=\"background-color:" + hex + "; padding:5px; -moz-border-radius:3px; -webkit-border-radius:3px;\">" + i + "</span>");
            }
            writer.close();
        }
    }

}
